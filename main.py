from tkinter import *
import os
root = Tk()
root.title("Hangman")
root.geometry("570x410")

def resource_path(relative_path):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")

    return os.path.join(base_path, relative_path)


root.iconbitmap(resource_path("icon.ico"))

def startScreen():
    def startGame():
        global word
        global gameOver
        x = 0
        for i in range(len(wordSV.get())):
            if wordSV.get()[i].isalpha() or wordSV.get()[i].isspace():
                continue
            else:
                x += 1
                break
        if x > 0:
            check = True
        else:
            check = False
        if wordSV.get() == "":
            wordEntry.config(bg="orange")
        elif check:
            wordEntry.config(bg="orange")

        else:
            wordEntry.config(bg="white")
            initialFrame.destroy()
            word = wordSV.get().strip(" ").upper()
            gameOver = False
            initialFrame.destroy()
            playGame(True)
            initializeGame()

    initialFrame = Frame(root, width=250, height=500)
    initialFrame.place(relx=.5, rely=.5, anchor='center')
    photo = PhotoImage(file=resource_path("logo.png"))
    label = Label(initialFrame, image=photo)
    label.img = photo
    label.pack()
    wordSV = StringVar()
    wordEntry = Entry(initialFrame, textvariable=wordSV, relief=SUNKEN, font="Helvetica 18 italic", justify=CENTER)
    button = Button(initialFrame, text="Start", font="Helvetica 15", relief=RAISED, command=startGame)
    wordEntry.pack(ipady=3)
    wordEntry.focus_force()
    button.pack()


def key(event):
    global gameOver
    global guesses
    if not gameOver:
        keypress = str(repr(event.char)).strip("'").upper()
        for i in range(26):
            if guesses[i] == " ":
                if keypress.isalpha() and keypress not in guesses:
                    guesses[i] = keypress
                    letterGuess(guesses)
                    playGame(False, keypress)
                break


def letterGuess(letter):
    global gameOver
    global letters
    global word
    if not gameOver:
        for i in range(26):
            if letter[i] != " ":
                letters[i].config(text=letter[i], relief=RAISED)


def playGame(first=False, letter=None):
    global hangmanImg
    global hangmanImages
    global wrongGuesses
    global rightGuesses
    global letterLabels
    global gameOver
    global word
    titleFrame = Frame(root, bg="blue")
    titleFrame.place(relx=.30, y=10)
    title = PhotoImage(file=resource_path("guess.png"))
    titleImage = Button(titleFrame, image=title, bd=5)
    titleImage.img = title
    titleImage.pack()
    if first:
        wordFrame = Frame(root, bg="blue", height="45")
        wordFrame.pack(side=BOTTOM, pady=(0, 25))
        for i in range(len(hangmanImg)):
            hangmanImg[i].place_forget()
        hangmanImg[0].img = hangmanImages[0]
        hangmanImg[0].place(relx=.35, rely=.35)
        for i in range(len(word)):
            if word[i].isalpha and not word[i].isspace():
                letterLabels.append(Label(wordFrame, text="_", font="Helvetica 18 italic", relief=RAISED, width=2))
            else:
                letterLabels.append(Label(wordFrame, text=word[i], font="Helvetica 18 italic", relief=RAISED, width=2))

        for i in range(len(word)):
            letterLabels[i].pack(anchor="center", side=LEFT)

    else:
        if letter not in word:
            if wrongGuesses < 6:
                wrongGuesses += 1
        else:
            for i in range(len(word)):
                if word[i] == letter:
                    rightGuesses += 1
                    letterLabels[i].config(text=letter)
        for i in range(len(hangmanImg)):
            hangmanImg[i].place_forget()
        hangmanImg[wrongGuesses].place(relx=.35, rely=.35)

        def dest():
            restart()
            titleFrame.destroy()

        if wrongGuesses >= 6:
            for i in range(len(word)):
                if letterLabels[i].cget("text") == "_":
                    letterLabels[i].config(text=word[i], bg="orange")
            gameOver = True
            lose = PhotoImage(file=resource_path("lose.png"))
            overImage = Button(titleFrame, image=lose, command=dest, bd=5)
            overImage.img = lose
            titleImage.pack_forget()
            overImage.pack()
            root.unbind("<Key>")
        wordwithoutspaces = word.replace(" ", "")
        if rightGuesses == len(wordwithoutspaces):
            win = PhotoImage(file=resource_path("win.png"))
            overImage = Button(titleFrame, image=win, command=dest, bd=5)
            overImage.img = win
            # overImage.place(relx=.25, rely=.10)
            titleImage.pack_forget()
            overImage.pack()
            root.unbind("<Key>")


def initializeGame():
    global guesses
    global letters
    guesses = [" " for i in range(26)]
    letterFrame = Frame(root, width=200, height=75)
    letterFrame.grid_propagate(True)
    letterFrame.pack(anchor=NW)
    guessedLabel = Label(letterFrame, text="Letters used: ", relief=RAISED, justify=CENTER)
    guessedLabel.grid(row=0, columnspan=13, sticky='w')
    letters = [Label(letterFrame, text=guesses[i].strip("'").upper(), relief=FLAT, width=2) for i in
               range(len(guesses))]
    counter = 0
    for i in range(4):
        x = 7
        if i == 3:
            x = 5
        for j in range(x):
            letters[counter].grid(row=i + 1, column=j)
            counter += 1
    root.bind("<Key>", key)
    root.focus_force()


def restart():
    def all_children(window):
        _list = window.winfo_children()

        for item in _list:
            if item.winfo_children():
                _list.extend(item.winfo_children())

        return _list

    widget_list = all_children(root)
    for item in widget_list:
        item.pack_forget()
        item.place_forget()

    global letterLabels
    global wrongGuesses
    global rightGuesses
    global hangmanImg
    global hangmanImages

    wrongGuesses = 0
    rightGuesses = 0
    del letterLabels[:]
    for i in range(7):
        hangmanImg.append(Label(root, image=hangmanImages[i]))
    startScreen()


gameOver = False
word = ""
letterLabels = []
wrongGuesses = 0
rightGuesses = 0
hangmanImages = [PhotoImage(file=resource_path("Hangman-" + str(i) + ".png")) for i in range(0, 7)]
hangmanImg = []
for i in range(7):
    hangmanImg.append(Label(root, image=hangmanImages[i]))
startScreen()
root.mainloop()
